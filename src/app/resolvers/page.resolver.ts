import { Resolve } from "@angular/router";
import { Block, TeamBlock } from "../interfaces/interfaces";
import { Injectable } from "@angular/core";
import { TeamService } from "../services/team.service";

@Injectable({
    providedIn: "root" 
})
export class PageResolver implements Resolve<Block<TeamBlock>[]>{

    constructor(private teamService: TeamService) { }

    resolve() {
        return this.teamService.getBlockData<TeamBlock>();
    }
}