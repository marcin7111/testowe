export class ServerData<T>{
    data:T[];
}

export class Block<T> {
    type: string;
    id: number;
    attributes: T;
}

export class TeamBlock {
    title: string;
    memberCards: TeamMembersCard;
}

export class TeamMembersCard {
    first?: TeamMembersCardData;
    second?: TeamMembersCardData;
    third?: TeamMembersCardData
}

export class TeamMembersCardData {
    imageUrl: Image;
    block: TeamMembersCardDataBlock;
}

export class TeamMembersCardDataBlock {
    title: string;
    description: string;
    link: string;
    text: string;
}

export class Image {
    w200: string;
    w400: string;
    w1080: string;
    w1920: string;
    w2560: string;
}