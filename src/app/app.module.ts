import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TeamComponent } from './blocks/team/team.component';
import { BlockHeadComponent } from './components/block-head/block-head.component';
import { ImageComponent } from './components/image/image.component';
import { MemberComponent } from './components/member/member.component';
import { PageResolver } from './resolvers/page.resolver';
import { PageComponent } from './components/page/page.component';

@NgModule({
  declarations: [
    AppComponent,
    TeamComponent,
    BlockHeadComponent,
    ImageComponent,
    MemberComponent,
    PageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [PageResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
