import { Component, Input } from '@angular/core';
import { TeamBlock } from '../../interfaces/interfaces';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent {

  private _teamData: TeamBlock;

  @Input() 
  set teamData(newData:TeamBlock){
    this._teamData = newData;
  }

  get teamData(){
    return this._teamData;
  }

  get title(){
    return this.teamData.title;
  }

  get first(){
    return this.teamData.memberCards.first;
  }

  get second(){
    return this.teamData.memberCards.second;
  }

  get third(){
    return this.teamData.memberCards.third;
  }
}
