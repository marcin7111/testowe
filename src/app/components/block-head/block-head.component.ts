import { Component } from '@angular/core';

@Component({
  selector: 'app-block-head',
  template: '<h2><ng-content></ng-content></h2>',
  styleUrls: ['./block-head.component.scss']
})
export class BlockHeadComponent { }
