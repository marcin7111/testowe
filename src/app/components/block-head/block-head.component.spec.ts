import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockHeadComponent } from './block-head.component';

describe('BlockHeadComponent', () => {
  let component: BlockHeadComponent;
  let fixture: ComponentFixture<BlockHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
