import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Block, TeamBlock } from '../../interfaces/interfaces';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent {

  blocks: Block<TeamBlock>;

  constructor(private route: ActivatedRoute) {
    this.blocks = this.route.snapshot.data['page'] as Block<TeamBlock>;
  }

}
