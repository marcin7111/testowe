import { Component, Input } from '@angular/core';
import { TeamMembersCardData } from '../../interfaces/interfaces';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent {

  @Input() data: TeamMembersCardData;

  get personalData(){
    return this.data.block;
  }

}
