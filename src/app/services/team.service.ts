import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServerData, Block } from '../interfaces/interfaces';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private httpClient:HttpClient) { }

  getBlockData<T>(){
    return this.httpClient.get<ServerData<Block<T>>>('https://cobiro-website-builder.s3-eu-west-1.amazonaws.com/task/index.json')
      .pipe(map(data=>{
        return data.data;
      }));
  }
}
